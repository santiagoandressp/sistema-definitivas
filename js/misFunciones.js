
let estudiantesTotal = [];
let listEstudiantes = document.getElementById("lista-estudiantes");
let aprobados = 0;
let desaprobados = 0;


  listEstudiantes.addEventListener("click",(evento)=>{
    
    if(evento.target.nodeName == "BUTTON"){
      let idhijo = evento.target.id;
       
      eliminarHijo(listEstudiantes,idhijo);
      removerEstudiante(idhijo);
      actualizarEst();
    }
  
  });

function adicionar(){

  const nombre = document.getElementById("nombre").value;
  const codigo = document.getElementById("codigo").value;
  const definitiva = document.getElementById("definitiva").value;
  
   if(comprobarFormulario(nombre,codigo,definitiva)){
    return false;
   }

   const padre = document.getElementById("lista-estudiantes");

    clear("nombre","codigo","definitiva");
    removerClase("btn-grafica","disabled");
    bloquearElementos("alert-msg");
    agregarEstudiante(padre,nombre,codigo,definitiva);

    let estudiante = new Estudiante(nombre,codigo,definitiva);
    estudiantesTotal.push(estudiante);

    actualizarEst();
}

function drawChart() {
    mostrarElementos("piechart-3d");
    document.getElementById("piechart-3d").style ="width: 900px; height: 300px";
    estApro();
    estDesapro();
    
    if(aprobados != 0 || desaprobados !=0 ){
      var data = google.visualization.arrayToDataTable([
        ["Estudiante","definitiva"],
        ["Estudiantes Aprobados",parseInt(aprobados)]
        ,["Estudiantes Desaprobados", parseInt(desaprobados)]]
        );
  
      var options = {
        title: 'Definitivas estudiantes',
        is3D: true
      };
  
      var chart = new google.visualization.PieChart(document.getElementById('piechart-3d'));
  
      document.getElementById("piechart-3d").style = "width: 900px; height: 500px; display: block;"
  
      chart.draw(data, options);
      return true;
    }else{
      bloquearElementos("piechart-3d");
    }
  }

  class Estudiante{

    constructor(nombre,codigo,definitiva){
      this.codigo = codigo;
      this.nombre = nombre;
      this.definitiva = definitiva;
    }
    toString(){
      return `\nNombre:${this.nombre}\n Código:${this.codigo}\n Definitiva:${this.definitiva}`
    }
  }


  // Funciones flecha adicionales

  let estApro = () => aprobados = estudiantesTotal.filter(estudiante => (estudiante.definitiva) >=3).length;


  let estDesapro = () => desaprobados =  estudiantesTotal.length-aprobados;

  let actualizarEst = () => document.getElementById("total-estudiantes").textContent = estudiantesTotal.length;

  let existeEstudiante  = (codigo) =>{

    let existe = false;

    if(estudiantesTotal.length == 0)
      return false;
    else{

      estudiantesTotal.forEach(estudiante=>{
        if(estudiante.codigo  == codigo){
          mostrarMsg(`Código repetido, estudiante: ${estudiante.nombre} Posee el mismo código`);
          existe = true;
          return true;
        }
      })
      return existe;
    }
    
  }

  let clear = (...elementos)=>elementos.map(elemento=> document.getElementById(elemento).value = "");
   

  let removerEstudiante =(codigo)=> estudiantesTotal= estudiantesTotal.filter(estudiante => estudiante.codigo!=codigo);

  let comprobarFormulario = (nombre, codigo, definitiva) =>{
    
    let vacios = comprobarVacios(nombre,codigo,definitiva);
    if(vacios) return true;

    let existe =existeEstudiante(codigo);
    if(existe) return true;

    if(codigo.length !=7){
      
      mostrarMsg("Código incorrecto, debe tener 7 dígitos.");
      return true;
    }
    
    if(parseFloat(definitiva)<0 || parseFloat(definitiva)>5){
      mostrarMsg("La definitiva debe ser menor o igual a 5 y mayor igual a 0");
      return true
    }

    return false;

  }

  let mostrarMsg = (msg)=>{
    mostrarElementos("alert-msg");
    document.getElementById("mensaje").textContent = msg;
  }

  let agregarEstudiante = (padre, nombre,codigo,definitiva)=>{   
    const estudiante = document.createElement("li");
    const color = calcularNota(definitiva);
    estudiante.id = codigo;
    estudiante.classList.add("list-group-item");

    estudiante.innerHTML = `<b>Nombre: </b> \n <span>${nombre}</span> \n <br> \n<b>Código: </b> \n <span id="codigo">${codigo}</span> \n <br> \n <b>Definitiva: </b> \n <code id="definitiva" class="${color}">${definitiva} </code>`;
    let boton = crearElemento("button",codigo,"btn", "X");
    boton.classList.add("btn-danger")


    agregarEstrellas(estudiante,definitiva,color );

    estudiante.appendChild(document.createElement("br"));
    agregarHijos(estudiante,boton);
    padre.appendChild(estudiante);
  
  }


  let agregarEstrellas=(padre,definitiva, color)=>{

    const definitivaD = parseInt(definitiva);
    const mediaEstrella = document.createElement("li");
    mediaEstrella.classList.add("fas");
    mediaEstrella.classList.add("fa-star-half-alt");
    mediaEstrella.classList.add(color);
    for (let i = 0; i < definitivaD; i++) {
      let estrellaC = document.createElement("li");
      estrellaC.classList.add("fas");
      estrellaC.classList.add("fa-star");
      estrellaC.classList.add(color);
      padre.appendChild(estrellaC)
    }

    const definitivaDecimal = definitivaD +0.5;

    if(definitiva >= definitivaDecimal){
      padre.appendChild(mediaEstrella);
    }

  }

  let calcularNota = (nota) =>{
    if(nota>=0 && nota <3)
      return "rojo";
    if(nota>=3 && nota<4)
      return "naranja";
    if(nota>=4 && nota<=5)
      return "verde";
  }

 // Primer parametro es el padre, el resto son los hijos
  let agregarHijos = (...elementos)=>{
    let i = 0;
    let padre;
    elementos.forEach(contenido => {
      if(i!=0){
        padre.appendChild(contenido);
      }else{
        padre = contenido;
      }
      i++;
    });
  }

  let eliminarHijo = (padre,idhijo)=>{
    padre.removeChild(document.getElementById(idhijo));
  }

  let crearElemento = (elemento,id,clase,contenido)=>{
    const elementoCreado = document.createElement(elemento);
    elementoCreado.classList.add(clase);
    elementoCreado.id = id;
    elementoCreado.textContent=contenido;
    return elementoCreado
  }

  let bloquearElementos = (...ids) => {

    ids.forEach(id =>{
        let elementoI = document.getElementById(id);
        elementoI.style ="display:none";  
    });


}

let comprobarVacios =(...parametros) =>{
    
  let isVacio = false;

  parametros.forEach(elemento =>{
      if(elemento.length == 0){
          mostrarMsg("¡Campos Vacíos!");
          isVacio =true;
          return true;
      }
  });

  return isVacio;
}

let removerClase = (id,clase)=>{
  document.getElementById(id).classList.remove(clase);
}

let agregarClase = (id,clase)=>{
  document.getElementById(id).classList.remove(clase);
}

let mostrarElementos = (...ids)=>{
    
    ids.forEach(id =>{
        let elementoI = document.getElementById(id);
        elementoI.style ="display:block";  
    });
}

let convFloat = (id) => {return parseFloat(document.getElementById(id).value)};
    